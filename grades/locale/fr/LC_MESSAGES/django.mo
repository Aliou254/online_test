��          �            x     y     �     �     �     �     �     �     �            
     K   *  ?   v     �     �    �     �             *   >  &   i     �     �     �     �     �  	   �  p   �  ]   ^     �     �        	                                                             
          Add a Grading System Add/Edit Course Add/Edit Grading Add/View Grading Systems Available Grading Systems: Description Grade Grading Ranges Grading System Lower Limit My Courses Note: For grade range lower limit is inclusive and upper limit is exclusive Note: This is a default grading system. You cannot change this. Upper Limit View Grading Systems Project-Id-Version: 0.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-07-13 13:30:31
Last-Translator: Server <root@localhost>
Language-Team: English <email_host_user>
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Ajouter un système de notation Ajouter/Modifier un cours Ajouter/Modifier une notation Ajouter/afficher des systèmes de notation Systèmes de classement disponibles : La description Noter Échelles de notation Système de notation Limite inférieure Mes cours Remarque : Pour la plage de niveaux, la limite inférieure est inclusive et la limite supérieure est exclusive Remarque : Il s'agit d'un système de notation par défaut. Vous ne pouvez pas changer cela. Limite supérieure Voir les systèmes de notation 